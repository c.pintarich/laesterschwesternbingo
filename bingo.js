"use Strict"

const spruechePhrasen = [
    'Trump twittert dummes Zeug',
    'HERR Newstime nennt sich selbst Journalist',
    'David macht einen Witz über Katja',
    'David erzählt von Nerdscope',
    'David spricht von Gamona',
    'David spricht von Giga',
    'Robin spricht von L.A.',
    'ROBERT HOFMANN(<3)',
    'David lacht über politisch inkorrekte Witze',
    'Robin findet etwas spannend',
    'Robin lacht über politisch inkorrekte Witze',
    'David unterbricht Robin',
    'Robin unterbricht David',
    'Werbung für Sky',
    'Werbung für Bookbeat',
    '"Das Büro" ist im Gespräch',
    'Robin erzählt von Nerdscope',
    '"Das Netflix der Hörbücher"',
    'David ist nicht mehr auf Facebook',
    'Montana Black News',
    'DIE Beschandsaufnahme',
    'Phillip DeFranco ist Thema',
    'Der Montana der Woche',
    'Robin spricht über ein Video von sich',
    'David spricht über ein Video von sich',
    'Jemand ist Alpha',
    'David liebt Aaron Troschke',
    'Robin lacht über ein unangebrachtes Thema',
    'David lacht über ein unangebrachtes Thema',
    'David erzählt Geschichten über Penisse',
    'David ist vom Thema genervt',
    'David unterbricht Robin',
    'Promiflash wird erwähnt',
    'Kollegah ist wieder Thema',
    'Katja Krasavice ist Thema',
    'Ein Influencer hat mal wieder was dummes gemacht',
    'Fabian Sigismund wird erwähnt',
    'Davids Webvideo Preis ist Thema',
    'Max (Hand of Blood) ist Thema',
    'Robin erwähnt Funk',
    'Es geht um unseriöse Werbeanfragen',
    'Richtig Cool GmbH',
    'David zieht über Filme her',
    'Promi BigBrother, egal wer!',
    'Die perversen von der Mansion',
];

const randomNumber = (max) => {
    return Math.floor(Math.random() * max);
}

const bingoGenerator = (val) => {
    $('#bingotable').empty();
    for (let t = 1; t <= val; t++) {
        let usedNumber = []
        $('<div>').attr('id', 'bingotable' + t).appendTo('#bingotable');
        $('<h1>').appendTo('#bingotable' + t).html('Das Lästerschwestern Bingo');
        $('<table>').attr('border', 1).appendTo('#bingotable' + t);
        $('<tr>').appendTo('#bingotable' + t + ' table');
        for (let i = 0; i <= 24; i++) {
            $('<td>').appendTo('#bingotable' + t + ' tr:last-child').attr('id', i).addClass('grid-style');
            if (i === 4 ||
                i === 9 ||
                i === 14 ||
                i === 19 ||
                i === 24) {
                $('<tr>').appendTo('#bingotable' + t + ' table');
            }
            let makeNumber = randomNumber(spruechePhrasen.length);
            if (!usedNumber.includes(makeNumber)) {
                usedNumber.push(makeNumber);
            } else {
                while (usedNumber.includes(makeNumber)) {
                    makeNumber = randomNumber(spruechePhrasen.length);
                    if (usedNumber.includes(makeNumber) === false) {
                        usedNumber.push(makeNumber);
                        break;
                    }
                }
            }
            if ($('#bingotable' + t + ' #' + i).is(':empty')) {
                $('#bingotable' + t + ' #' + i).html(spruechePhrasen[makeNumber]);
            }

        }
        $('<div>').addClass('page-break').appendTo('#bingotable');
    }
}

$('<div>')
    .addClass('form-group')
    .appendTo('#bingotable');
$('<label>')
    .attr('for', 'm')
    .html('Wieviele Spiele möchtest du haben?')
    .appendTo('#bingotable');
$('<input>')
    .appendTo('#bingotable')
    .attr({
        'type': 'text',
        'class': 'form-control',
        'id': 'inp',
        'placeholder': 'Gib eine Zahl ein'
    });
const button = $('<button>')
    .appendTo('#bingotable')
    .html('Los');

$(button).on('click', () => {
    const stueck = $('#inp').val();
    $('#bingotable').empty();
    $('<div>').appendTo('#bingotable').addClass('spinner-grow').attr('role', 'status');
    $('<span>').appendTo('.spinner-grow').addClass('sr-only').html('Lade...');
    bingoGenerator(stueck);
});